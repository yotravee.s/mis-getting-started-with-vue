# mis-getting-started-with-vue

**Getting Started with Vue.js by Yotravee Sanit-in**

###### Setup Environment

- Install Node.js and NPM [Node.js Page](https://nodejs.org/en/download/).
- Install Vue CLI (npm install -g @vue/cli || yarn global add @vue/cli)

Initial Vue.js Project:

```
cd mis-getting-started-with-vue
cd vue-1
npm install
```

Test run serve for development:

```
npm run serve
```

Note that the development build is not optimized.
You can build a project for production, run

```
npm run build
```

In case of vue-2 project, You can do like the above steps.

###### :sparkles: Let's enjoy Vue.js Tutorial. :sparkles: