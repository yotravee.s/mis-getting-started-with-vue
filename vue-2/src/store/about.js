const state = {
    count_about: 0
}

const getters = {
    getTextCount (state) {
        return 'Array count: ' + state.count_about
    }
}

export default {
    state,
    getters
}